removeStupidJbzdyBlocker = function(blurredElementSelector, popupElementSelector) {
    const blurredElement = document.getElementById(blurredElementSelector);

    if (null !== blurredElement) {
        blurredElement.classList.remove('blur');
    }

    const popupElement = document.getElementById(popupElementSelector);

    if (null !== popupElement) {
        popupElement.style.display = 'none';
    }

    console.log('I have run the JBZDY extension. Should be clear now');
};

removeStupidJbzdyBlocker('content-wrapper', 'a-campaign-board');
